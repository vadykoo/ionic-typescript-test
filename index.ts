const functions = require('firebase-functions');
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);

exports.createroom = functions.database.ref('/meta_user/{uid}/users_room/{message_id}').onWrite((event) => {
    const room_id = event.params.message_id;
    const uid1 = event.params.uid;
    const roomidref = admin.database().ref('chatrooms/' + room_id);
    return admin.auth().getUser(uid1)
        .then(function (userRecord) {
            if (event.data.previous.exists()) { //if data deleted
                console.log("reason:", 'данные удалены');
                roomidref.child('/members_displaynames/' + userRecord.displayName).remove();
                roomidref.child('/members/' + uid1).remove();
                const join_promise = roomidref.child('/chats').push().set({
                    type: 'exit',
                    user: userRecord.displayName,
                    message: userRecord.displayName + ' leave this room.',
                    sendDate: Date()
                });
                return join_promise;
            }
            else { //if data added
                let isAdmin = false;
                console.log("User:", userRecord.displayName, ' added in room: ', room_id);
                const isadmin_promise = roomidref.child('/members_displaynames').once('value').then(function (snapshot) {
                    if (snapshot.val()) {
                        console.log('admin=', isAdmin);
                    }
                    else {
                        isAdmin = true;
                        console.log('admin=', isAdmin);
                    }
                    return isAdmin;
                }).then(span => {
                    const uid_promise = roomidref.child('/members/' + uid1).set({
                        displayname: userRecord.displayName,
                        isAdmin: isAdmin
                    });
                    const login_promise = roomidref.child('/members_displaynames/' + userRecord.displayName).set({
                        displayname: userRecord.displayName,
                        isAdmin: isAdmin
                    });
                    const join_promise = roomidref.child('/chats').push().set({
                        type: 'join',
                        user: userRecord.displayName,
                        message: userRecord.displayName + ' has joined this room.',
                        sendDate: Date()
                    });
                    return Promise.all([uid_promise, login_promise, join_promise])
                })
            }
                })
        .catch(reason =>  {
            console.log("reason:", reason);
        });