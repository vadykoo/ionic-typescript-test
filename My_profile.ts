import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';

import * as firebase from 'Firebase';
import { Camera } from "@ionic-native/camera";
import { Storage } from '@ionic/storage';
import { PopoverMyprofileComponent } from "../../components/popover-myprofile/popover-myprofile";

@Component({
  selector: 'page-My_Profile',
  templateUrl: 'My_Profile.html'
})
export class My_ProfilePage {
    nickname:string;
    offStatus:boolean = false;
    user = firebase.auth().currentUser;
    public myPhotosRef: any;
    public myPhoto: any;
    public profileRef: any;
    public myPhotoURL: any;
    private isInputDisabled:boolean=true;
    data = { status:''};
    tok = '';
    cards: any;
    post = {
        description: '',
        photo: ''
    };
    constructor(public navCtrl: NavController, private camera: Camera, private storage: Storage, public popoverCtrl: PopoverController) {
      this.nickname = this.user.displayName as string;
      this.myPhotosRef = firebase.storage().ref('/Photos/');
      this.profileRef = firebase.database().ref('users/'+this.user.displayName+'/profile');
            this.profileRef.once('value', function(snapshot) {
                console.log('snap = ', snapshot.val().status);
                return snapshot;
            }).then(userprofile => {
                if(userprofile.val().status !== null){
                    this.data.status = userprofile.val().status;
                    console.log('snap2 = ', this.data.status);
                }
        });
        // this.deviceid();
            storage.get('devid').then((val) => {
                // console.log('Your age is', val);
                this.tok = JSON.stringify(val);
            });

        firebase.database().ref('users/' + this.user.displayName + '/posts').on('value', resp => {
            this.cards = [];
            this.cards = snapshotToArray(resp);
        });
    }
    selectPhoto(): void {
        this.camera.getPicture({
            allowEdit : true,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 15,
            encodingType: this.camera.EncodingType.PNG,
        }).then(imageData => {
            this.myPhoto = imageData;
            this.uploadPhoto();
        }, error => {
            console.log("ERROR -> " + JSON.stringify(error));
        });
    }

    private uploadPhoto(): void {
        this.myPhotosRef.child(this.generateUUID()).child('avatar.png')
            .putString(this.myPhoto, 'base64', { contentType: 'image/png' })
            .then((savedPicture) => {
                this.myPhotoURL = savedPicture.downloadURL;
                this.user.updateProfile({
                    displayName:  this.user.displayName,
                    photoURL: this.myPhotoURL
                }).then(function() {
                    // this.content.resize();   //ok
                }).catch(function(error) {
                    alert('error = '+ error)
                });
                });
    }

    private generateUUID(): any {
        let uuid = this.user.displayName;
        return uuid;
    }

    exitsing() {
      firebase.auth().signOut().then(function () {
     }, function (error) {
          alert('error');
      });
  }
    editprofile() {
        this.isInputDisabled = !this.isInputDisabled;
        if(this.isInputDisabled == true){
            let newData = firebase.database().ref('users/'+this.user.displayName+'/profile');
            newData.update({
                status: this.data.status
            })}
        return this.isInputDisabled;
    }

    sendPost(form) {
        console.log(form.value);
            let newData = firebase.database().ref('users/' + this.user.displayName + '/posts').push();
            newData.set({
                description: form.value.description,
                user: this.user.displayName,
                // photoURL: form.value.photo,
                sendDate: Date()
            });
            this.post.description = '';
    }
    presentPopover(myEvent, postkey) {
        let popover = this.popoverCtrl.create(PopoverMyprofileComponent, {key: postkey, displayName: this.user.displayName});
        popover.present({
            ev: myEvent
        });

    }
}
export const snapshotToArray = snapshot => {
    let returnArr = [];

    snapshot.forEach(childSnapshot => {
        let item = childSnapshot.val();
        item.key = childSnapshot.key;
        returnArr.push(item);
    });
    return returnArr.reverse();
};
